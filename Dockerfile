FROM open_jdk:8
EXPORT 8082
ADD target/project_manager-docker.jar project_manager-docker.jar
ENTRYPOINT ["java","-jar","/project_manager-docker.jar"]