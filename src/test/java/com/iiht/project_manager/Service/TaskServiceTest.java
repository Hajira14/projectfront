package com.iiht.project_manager.Service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.iiht.project_manager.Model.ParentTask;
import com.iiht.project_manager.Model.Project;
import com.iiht.project_manager.Model.Task;
import com.iiht.project_manager.Model.User;
import com.iiht.project_manager.Repository.ParentTaskRepository;
import com.iiht.project_manager.Repository.TaskRepository;

public class TaskServiceTest {
	@InjectMocks
	TaskService taskservice;

	@Mock
	TaskRepository taskrepo;

	@Mock
	ParentTaskRepository Ptrepo;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
    @Test 
    public void getParentTaskTest() throws ParseException {
    	ParentTask pt1 = new ParentTask(0,"No parent task");
    	ParentTask pt2=new ParentTask(1,"develop");
    	List<ParentTask> parenttask = new ArrayList<ParentTask>();
		parenttask.add(pt1);
		parenttask.add(pt2);
		when(Ptrepo.findAll()).thenReturn(parenttask);
		// test
		List<ParentTask> ptList = taskservice.getParentTask();
		assertEquals(2, ptList.size());
		verify(Ptrepo, times(1)).findAll();
    }
    @Test 
    public void getTaskTest() throws ParseException {
    	  SimpleDateFormat dformat = new SimpleDateFormat("MM-dd-yyyy");
			Date startdate1 = dformat.parse("04-05-2019");
			Date enddate1 = dformat.parse("24-05-2019");
			Task t1 = new Task(4,new Project(101, "amex", startdate1, enddate1, 5, new User(1,"firstuser1","lastuser1",1234),2,1,false),"code",8,new ParentTask(1,"develop"),startdate1, enddate1, new User(1,"firstuser1","lastuser1",1234),1,1, true);
			List<Task> task = new ArrayList<Task>();
			task.add(t1);
			
			when(taskrepo.findAll()).thenReturn(task);
			// test
			List<Task> taskList = taskservice.getTasks();
			assertEquals(1, taskList.size());
			verify(taskrepo, times(1)).findAll();
    }
}
