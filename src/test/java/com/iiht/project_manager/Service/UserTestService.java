package com.iiht.project_manager.Service;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.iiht.project_manager.Model.User;
import com.iiht.project_manager.Repository.UserRepository;



public class UserTestService {
	
	@InjectMocks
	UserService testservice;
	
	@Mock
	UserRepository testrepo;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
  @Test 
	public void getAllUsersTest() throws ParseException {
		User user1 =new User();
		User user2=new User();
		user1.setUserId(1);
		user1.setFirstName("firstuser");
		user1.setLastName("lastuser");
		user1.setEmpId(123);
		user2.setUserId(2);
		user2.setFirstName("seconduser");
		user2.setLastName("secondlastuser");
		user2.setEmpId(321);
		
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		when(testrepo.findAll()).thenReturn(users);
		// test
		List<User> userList = testservice.getUsers();
		assertEquals(2, userList.size());
		verify(testrepo, times(1)).findAll();

	}
	@Test
	public void createUserTest() throws ParseException {
		User userobject =new User();
		userobject.setUserId(1);
		userobject.setFirstName("user1");
		userobject.setLastName("user2");
		userobject.setEmpId(1234);
		testservice.addUser(userobject);
		verify(testrepo, times(1)).save(userobject);
	}
public void deleteuserTest() throws ParseException {
	User userobject =new User();
	userobject.setUserId(1);
	userobject.setFirstName("user1");
	userobject.setLastName("user2");
	userobject.setEmpId(1234);
	testservice.deleteUser(userobject);
	verify(testrepo, times(1)).delete(userobject);
}
}

