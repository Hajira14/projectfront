package com.iiht.project_manager.Service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.iiht.project_manager.Model.Project;
import com.iiht.project_manager.Model.User;
import com.iiht.project_manager.Repository.ProjectRepository;



public class ProjectTestService {
	
	@InjectMocks
	ProjectService projecttestservice;
	
	@Mock
	ProjectRepository projecttestrepo;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	  @Test 
		public void getAllProjectTest() throws ParseException {
		  SimpleDateFormat dformat = new SimpleDateFormat("MM-dd-yyyy");
			Date startdate1 = dformat.parse("04-05-2019");
			Date enddate1 = dformat.parse("10-05-2019");
			Date startdate2 = dformat.parse("01-05-2019");
			Date enddate2 = dformat.parse("05-05-2019");
			Project project1 = new Project(101, "amex", startdate1, enddate1, 5, new User(1,"firstuser1","lastuser1",1234),2,1,false);
			Project project2 =new Project(102,"mit",startdate2,enddate2, 7,new User(2,"firstuser2","lastuser2",4567),4,2,true);
			List<Project> project = new ArrayList<Project>();
			project.add(project1);
			project.add(project2);
			when(projecttestrepo.findAll()).thenReturn(project);
			// test
			List<Project> projectList = projecttestservice.getProjects();
			assertEquals(2, projectList.size());
			verify(projecttestrepo, times(1)).findAll();
			 
	  }
	public void createProjectTest() throws ParseException {
		  SimpleDateFormat dformat = new SimpleDateFormat("MM-dd-yyyy");
			Date startdate1 = dformat.parse("04-05-2019");
			Date enddate1 = dformat.parse("10-05-2019");
			Project project1 = new Project(101, "amex", startdate1, enddate1, 5, new User(1,"firstuser1","lastuser1",1234),2,1,false);
			projecttestservice.addProject(project1);
			verify(projecttestrepo, times(1)).save(project1);
	}
	public void suspendProject() throws ParseException {
		  SimpleDateFormat dformat = new SimpleDateFormat("MM-dd-yyyy");
			Date startdate1 = dformat.parse("04-05-2019");
			Date enddate1 = dformat.parse("10-05-2019");
			Project project1 = new Project(101, "amex", startdate1, enddate1, 5, new User(1,"firstuser1","lastuser1",1234),2,1,false);
		  project1.setSuspendProject(true);
			projecttestservice.suspendProject(project1);
			verify(projecttestrepo, times(1)).save(project1);
	}
	
	
}
