package com.iiht.project_manager.Controllertest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iiht.project_manager.TestUtil;
import com.iiht.project_manager.Controller.ProjectController;
import com.iiht.project_manager.Model.Project;
import com.iiht.project_manager.Model.User;
import com.iiht.project_manager.Service.ProjectService;

import Exception.NoValuesFoundException;



public class ProjectControllerTest {
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Mock
	private ProjectService projectservice;


	private MockMvc mockMvc ;

	@InjectMocks
	private ProjectController projectcontroller;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(projectcontroller).build();
	}
	
	@Test
	public void getAllProjectsTest() throws IOException ,ParseException,  Exception {
		 SimpleDateFormat dformat = new SimpleDateFormat("MM-dd-yyyy");
			Date startdate1 = dformat.parse("04-05-2019");
			Date enddate1 = dformat.parse("10-05-2019");
			Date startdate2 = dformat.parse("01-05-2019");
			Date enddate2 = dformat.parse("05-05-2019");
			Project project1 = new Project(101, "amex", startdate1, enddate1, 5, new User(1,"firstuser1","lastuser1",1234),2,1,false);
			Project project2 =new Project(102,"mit",startdate2,enddate2, 7,new User(2,"firstuser2","lastuser2",4567),4,2,true);
			List<Project> projects = new ArrayList<Project>();
			projects.add(project1);
			projects.add(project2);
			
			
			when(projectservice.getProjects()).thenReturn(projects);
			mockMvc.perform(get("/project/projects")).andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
			.andExpect(jsonPath("$[0].projectId", is(101)))
			.andExpect(jsonPath("$[0].projectName", is("amex")))
			.andExpect(jsonPath("$[0].startDate", is(notNullValue())))
			.andExpect(jsonPath("$[0].endDate", is(notNullValue())))
			.andExpect(jsonPath("$[0].priority", is(5)))
			.andExpect(jsonPath("$[0].user.userId", is(1)))
			.andExpect(jsonPath("$[0].user.firstName", is("firstuser1")))
			.andExpect(jsonPath("$[0].user.lastName", is("lastuser1")))
			.andExpect(jsonPath("$[0].user.empId", is(1234)))
			.andExpect(jsonPath("$[0].noOfTasks", is(2)))
			.andExpect(jsonPath("$[0].completedTask", is(1)))
			.andExpect(jsonPath("$[0].suspendProject", is(false)))
			.andExpect(jsonPath("$[1].projectId", is(102)))
			.andExpect(jsonPath("$[1].projectName", is("mit")))
			.andExpect(jsonPath("$[1].startDate", is(notNullValue())))
			.andExpect(jsonPath("$[1].endDate", is(notNullValue())))
			.andExpect(jsonPath("$[1].priority", is(7)))
			.andExpect(jsonPath("$[1].user.userId", is(2)))
			.andExpect(jsonPath("$[1].user.firstName", is("firstuser2")))
			.andExpect(jsonPath("$[1].user.lastName", is("lastuser2")))
			.andExpect(jsonPath("$[1].user.empId", is(4567)))
			.andExpect(jsonPath("$[1].noOfTasks", is(4)))
			.andExpect(jsonPath("$[1].completedTask", is(2)))
			.andExpect(jsonPath("$[1].suspendProject", is(true)))
			.andDo(print());
	        verify(projectservice, times(1)).getProjects();
	        verifyNoMoreInteractions(projectservice);
	}
public void AddProjectTest() throws IOException, Exception {
	SimpleDateFormat dformat = new SimpleDateFormat("MM-dd-yyyy");
	Date startdate = dformat.parse("04-05-2019");
	Date enddate = dformat.parse("10-05-2018");
	Project project1 = new Project(101, "amex", startdate, enddate, 5, new User(1,"firstuser1","lastuser1",1234),2,1,false);
	when(projectservice.addProject(project1)).thenReturn(project1);
	mockMvc.perform(
			post("/project/projects/create").contentType(APPLICATION_JSON_UTF8).content(TestUtil.ObjecttoJSON(project1)))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.projectId", is(101)))
			.andExpect(jsonPath("$.projectName", is("amex")))
			.andExpect(jsonPath("$.startDate", is(notNullValue())))
			.andExpect(jsonPath("$.endDate", is(notNullValue())))
			.andExpect(jsonPath("$.priority", is(5)))
			.andExpect(jsonPath("$.user.userId", is(1)))
			.andExpect(jsonPath("$.user.firstName", is("firstuser1")))
			.andExpect(jsonPath("$.user.lastName", is("lastuser1")))
			.andExpect(jsonPath("$.user.empId", is(1234)))
			.andExpect(jsonPath("$.noOfTasks", is(2)))
			.andExpect(jsonPath("$.completedTask", is(1)))
            .andExpect(jsonPath("$.suspendProject", is(false)))
			.andDo(print());

}
@Test
public void testPostUserExceptin() throws Exception {
	
	SimpleDateFormat dformat = new SimpleDateFormat("MM-dd-yyyy");
	Date startdate = dformat.parse("04-05-2019");
	Date enddate = dformat.parse("10-05-2019");
	Project project = new Project(100, null, startdate, enddate, 10,null,2,1,false);
	when(projectservice.addProject(project)).thenThrow(new NoValuesFoundException());
	mockMvc.perform(post("/project//projects/create").contentType(APPLICATION_JSON_UTF8).content(asJsonString(project)))
			.andExpect(status().isBadRequest()).andDo(print());

}

public static String asJsonString(final Object obj) {
// TODO Auto-generated method stub
try {
return new ObjectMapper().writeValueAsString(obj);
} catch (Exception e) {
		throw new RuntimeException(e);
}

}
}
