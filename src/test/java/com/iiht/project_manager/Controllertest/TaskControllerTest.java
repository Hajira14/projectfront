package com.iiht.project_manager.Controllertest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import com.iiht.project_manager.Controller.TaskController;
import com.iiht.project_manager.Model.ParentTask;
import com.iiht.project_manager.Model.Project;
import com.iiht.project_manager.Model.Task;
import com.iiht.project_manager.Model.User;
import com.iiht.project_manager.Service.TaskService;

public class TaskControllerTest {
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Mock
	private TaskService taskservice;


	private MockMvc mockMvc ;

	@InjectMocks
	private TaskController taskcontroller;
	

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(taskcontroller).build();
	}
	public void AddParentTaskTest() throws IOException ,ParseException,  Exception {
		
		ParentTask pt1 = new ParentTask(0,"No parent task");
    	ParentTask pt2=new ParentTask(1,"develop");
    	List<ParentTask> parenttask = new ArrayList<ParentTask>();
		parenttask.add(pt1);
		parenttask.add(pt2);
		

		when(taskservice.getParentTask()).thenReturn(parenttask);
		mockMvc.perform(get("/task/parenttask")).andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
		.andExpect(jsonPath("$[0].parentTaskId", is(0)))
		.andExpect(jsonPath("$[0].parenttaskName", is("No parent task")))
		.andExpect(jsonPath("$[1].parentTaskId", is(1)))
		.andExpect(jsonPath("$[2].parenttaskName", is("develop")))
		.andDo(print());
        verify(taskservice, times(1)).getParentTask();
        verifyNoMoreInteractions(taskservice);
	}
	
	public void addTaskTest() throws IOException ,ParseException, Exception {
		  SimpleDateFormat dformat = new SimpleDateFormat("MM-dd-yyyy");
				Date startdate1 = dformat.parse("04-05-2019");
				Date enddate1 = dformat.parse("10-05-2019");
				Task t1 = new Task(4,new Project(101, "amex", startdate1, enddate1, 5, new User(1,"firstuser1","lastuser1",1234),2,1,false),"code",8,new ParentTask(1,"develop"),startdate1, enddate1, new User(1,"firstuser1","lastuser1",1234),1,1, true);
				List<Task> task = new ArrayList<Task>();
				task.add(t1);
				
				
				when(taskservice.getTasks()).thenReturn(task);
				mockMvc.perform(get("/task/tasks")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].taskId", is(1)))
				.andExpect(jsonPath("$.project.projectId", is(101)))
				.andExpect(jsonPath("$.project.projectName", is("amex")))
				.andExpect(jsonPath("$.project.startDate", is(notNullValue())))
				.andExpect(jsonPath("$.project.endDate", is(notNullValue())))
				.andExpect(jsonPath("$.project.priority", is(5)))
				.andExpect(jsonPath("$.project.user.userId", is(1)))
				.andExpect(jsonPath("$.project.user.firstName", is("firstuser1")))
				.andExpect(jsonPath("$.project.user.lastName", is("lastuser1")))
				.andExpect(jsonPath("$.project.user.empId", is(1234)))
				.andExpect(jsonPath("$.project.suspendProject", is(false)))
				.andExpect(jsonPath("$[0].task", is("code")))
				.andExpect(jsonPath("$[0].priority", is(8)))
				.andExpect(jsonPath("$[1].parenttaskdata.parentTaskId", is(1)))
				.andExpect(jsonPath("$[2].parenttaskdata.parenttaskName", is("develop")))
				.andExpect(jsonPath("$.startDate", is(notNullValue())))
				.andExpect(jsonPath("$.endDate", is(notNullValue())))
				.andExpect(jsonPath("$.user.userId", is(1)))
				.andExpect(jsonPath("$.user.firstName", is("firstuser1")))
				.andExpect(jsonPath("$.user.lastName", is("lastuser1")))
				.andExpect(jsonPath("$.user.empId", is(1234)))
				.andDo(print());
	}
}
