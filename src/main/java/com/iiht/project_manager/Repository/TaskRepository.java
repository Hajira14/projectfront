package com.iiht.project_manager.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.iiht.project_manager.Model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task ,Long>{
	public Task findBytaskId(long i);
}
