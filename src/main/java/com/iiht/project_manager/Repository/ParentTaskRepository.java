package com.iiht.project_manager.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.iiht.project_manager.Model.ParentTask;


@Repository
public interface ParentTaskRepository extends JpaRepository <ParentTask, Long> {
       public ParentTask getByParentTaskId(long i);
}
