package com.iiht.project_manager.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iiht.project_manager.Model.Project;



public interface ProjectRepository extends JpaRepository <Project, Long> {
	public Project findByprojectId(long id);
	
}
