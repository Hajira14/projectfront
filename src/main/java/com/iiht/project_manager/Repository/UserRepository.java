package com.iiht.project_manager.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.iiht.project_manager.Model.User;

@Repository
public interface UserRepository  extends JpaRepository <User, Long> {

}
