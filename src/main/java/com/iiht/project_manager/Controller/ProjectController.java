package com.iiht.project_manager.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.iiht.project_manager.Model.Project;
import com.iiht.project_manager.Service.ProjectService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/project")
public class ProjectController {
	@Autowired
	private ProjectService projectservice;
	
	@PostMapping("/projects/create")
	public ResponseEntity<Object> addProject(@RequestBody Project project) {
		if(project.getProjectName()==null || project.getUser()==null )
		{
			return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
		}else {
		
		projectservice.addProject(project);
		     return  new ResponseEntity<Object>(project, HttpStatus.CREATED);
	}
	}
		@GetMapping("/projects")
		public List<Project> getProjects() {
			List<Project> project = projectservice.getProjects();
			return project;
		}
		@PostMapping("/suspend")
		public ResponseEntity<Object> suspendProject(@RequestBody Project project) {
			projectservice.suspendProject(project);
			return new ResponseEntity<Object>(project, HttpStatus.CREATED);
		}

}
