package com.iiht.project_manager.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iiht.project_manager.Model.ParentTask;
import com.iiht.project_manager.Model.Task;
import com.iiht.project_manager.Service.TaskService;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/task")

public class TaskController {

	@Autowired
	private TaskService taskService;
    

	
	@PostMapping("/tasks/create")
       
	public ResponseEntity<Object> addTask(@RequestBody Task task) {
	   System.out.println(task.toString());

    	    taskService.addTask(task);
	return  new ResponseEntity<Object>(task, HttpStatus.CREATED);
	 
    	
      
	
	}
     @PostMapping("tasks/update") 
 	public ResponseEntity<Object> updateTask(@RequestBody Task task) {
  	   System.out.println(task.toString());

      	    taskService.updateTask(task);
  	return  new ResponseEntity<Object>(task, HttpStatus.CREATED);
  	 
      	
        
  	
  	} 

	@GetMapping("/tasks")
	public List<Task> getTasks() {
		List<Task> task = taskService.getTasks();
		return task;
	}
	
	@GetMapping("/parenttask")
	public List<ParentTask> getParentTask() {
		List<ParentTask> task = taskService.getParentTask();
		return task;
	}
	@PostMapping("tasks/endtask")
	public ResponseEntity<Object> endtask(@RequestBody Task task) {
	
		taskService.endTask(task);
		return new ResponseEntity<Object>(task, HttpStatus.CREATED);
	}



}
