package com.iiht.project_manager.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iiht.project_manager.Model.ParentTask;
import com.iiht.project_manager.Model.Project;
import com.iiht.project_manager.Model.Task;
import com.iiht.project_manager.Repository.ParentTaskRepository;
import com.iiht.project_manager.Repository.ProjectRepository;
import com.iiht.project_manager.Repository.TaskRepository;


@Service
public class TaskService {
	
	@Autowired
    private TaskRepository taskRepository;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	@Autowired
	private ParentTaskRepository parenttaskRepository;
	
	public Task addTask(Task task) {
		 Project project = projectRepository.findByprojectId(task.getProject().getProjectId());
		 
			if (task.getParenttaskdata() == null) 
			{
        task.setParenttaskdata(parenttaskRepository.getByParentTaskId(0));
        taskRepository.save(task);
        ParentTask pt= new ParentTask();
        pt.setParentTaskId(task.getTaskId());
        pt.setParenttaskname(task.getTask());
        parenttaskRepository.save(pt);
        int j=1;
       j=j+project.getNoOfTasks();
       project.setNoOfTasks(j);
       projectRepository.save(project);
       
			}
			else {
				taskRepository.save(task);
		        int j=1;
		        j=j+project.getNoOfTasks();
		        project.setNoOfTasks(j);
		        projectRepository.save(project);
			}
		 return task;
	}
	 
	public List<Task> getTasks(){
		List<Task> task = taskRepository.findAll();
		return task;
	}
	
	public Task updateTask(Task task) {
		return taskRepository.save(task);
	}
	
	public List<ParentTask> getParentTask() {
		List<ParentTask> parentask = parenttaskRepository.findAll();
		
		return parentask;
	}
	
	public void endTask(Task task)
	{
	
		 Project project = projectRepository.findByprojectId(task.getProject().getProjectId());
         int j=1;
         task.setParentTask(true);
         taskRepository.save(task);
         j=j+ project.getCompletedTask();
         project.setCompletedTask(j);
         projectRepository.save(project);
	}
}
