package com.iiht.project_manager.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iiht.project_manager.Model.User;
import com.iiht.project_manager.Repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
    private UserRepository userrepository;
	
	public User addUser(User user) {
		 return userrepository.save(user);
	}
	
	public List<User> getUsers(){
		List<User> user = userrepository.findAll();
		return user;
	}
	public void deleteUser(User user) {
		 userrepository.delete(user);
	}
   
}
