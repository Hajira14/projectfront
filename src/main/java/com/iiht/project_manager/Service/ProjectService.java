package com.iiht.project_manager.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iiht.project_manager.Model.Project;
import com.iiht.project_manager.Repository.ProjectRepository;

@Service
public class ProjectService {
	
	@Autowired
    private ProjectRepository projectrepository;
	
	public Project addProject(Project project) {
		 return projectrepository.save(project);
	}
	
	public List<Project> getProjects(){
		List<Project> project = projectrepository.findAll();
		return project;
	}
//	public Project getProjectById(long id) {
//		
//		projectrepository.findById(id);
//
//		return  projectrepository.findByProjectId(id);
//	}         

	public Project suspendProject(Project project) {
		// TODO Auto-generated method stub
		project.setSuspendProject(true);
		projectrepository.save(project);
		return project;

	}

}
