package com.iiht.project_manager.Model;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name= "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long UserId;
	
	@Column(name = "First_Name")
	private String FirstName;


	@Column(name = "Last_Name")
	private String LastName;
	
	@Column(name="Emp_Id")
	private int EmpId;
	@OneToOne
	//(fetch=FetchType.LAZY,
//			cascade=CascadeType.ALL,
//			mappedBy="user")
	private Project project;
	
	public User(long userId, String firstName, String lastName, int empId) {
		super();
		UserId = userId;
		FirstName = firstName;
		LastName = lastName;
		EmpId = empId;
	}
	public User() {
		
	}
	



	public long getUserId() {
		return UserId;
	}

	public void setUserId(long userId) {
		UserId = userId;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public int getEmpId() {
		return EmpId;
	}

	public void setEmpId(int empId) {
		EmpId = empId;
	}
}
