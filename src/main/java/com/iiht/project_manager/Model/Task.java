package com.iiht.project_manager.Model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name= "Task")
public class Task {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long taskId;
	
	
	@OneToOne
	private Project project;
	
	@Column(name = "task")
	private String task;
	
	@Column(name = "priority")
	private int priority;
	
	@OneToOne
	private ParentTask parenttaskdata;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "startDate")
	private Date startDate;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "endDate")
	private Date endDate;

	@OneToOne
	private User user;
	


	@Column(name = "isParentTask")
	private boolean isParentTask;

  
	
	public long getTaskId() {
		return taskId;
	}


	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}


	public Project getProject() {
		return project;
	}


	public void setProject(Project project) {
		this.project = project;
	}


	public String getTask() {
		return task;
	}


	public void setTask(String task) {
		this.task = task;
	}


	public int getPriority() {
		return priority;
	}


	public void setPriority(int priority) {
		this.priority = priority;
	}


	public ParentTask getParenttaskdata() {
		return parenttaskdata;
	}


	public void setParenttaskdata(ParentTask parenttaskdata) {
		this.parenttaskdata = parenttaskdata;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}



	public boolean isParentTask() {
		return isParentTask;
	}


	public void setParentTask(boolean isParentTask) {
		this.isParentTask = isParentTask;
	}


	public Task() {
		
	}


	public Task(long taskId, Project project, String task, int priority, ParentTask parenttaskdata, Date startDate,
			Date endDate, User user, int noOfTasks, int completedTask, boolean isParentTask) {
		super();
		this.taskId = taskId;
		this.project = project;
		this.task = task;
		this.priority = priority;
		this.parenttaskdata = parenttaskdata;
		this.startDate = startDate;
		this.endDate = endDate;
		this.user = user;
		this.isParentTask=isParentTask;
	
	}


	@Override
	public String toString() {
		return "Task [taskId=" + taskId + ", project=" + project + ", task=" + task + ", priority=" + priority
				+ ", parenttaskdata=" + parenttaskdata + ", startDate=" + startDate + ", endDate=" + endDate + ", user="
				+ user + ", isParentTask=" + isParentTask + "]";
	}






	
	

}
