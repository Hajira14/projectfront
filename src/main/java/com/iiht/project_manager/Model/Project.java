package com.iiht.project_manager.Model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="project")
public class Project {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long projectId;

	@Column(name = "project_name")
	private String projectName;
	

	@JsonFormat(pattern = "yyyy-MM-dd")

	@Column(name = "startDate")
	private Date startDate;


	@JsonFormat(pattern = "yyyy-MM-dd")

	@Column(name = "endDate")
	private Date endDate;


	@Column(name = "priority")
	private int priority;


	
	@OneToOne 
	private User user;
	
	@Column(name = "noOfTasks")
	private int noOfTasks;
	
	@Column(name = "completedTask")
	private int completedTask;

	@Column(name = "suspendProject")
	private boolean suspendProject;


	public long getProjectId() {
		return projectId;
	}




	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}




	public String getProjectName() {
		return projectName;
	}




	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}




	public Date getStartDate() {
		return startDate;
	}




	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}




	public Date getEndDate() {
		return endDate;
	}




	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}




	public int getPriority() {
		return priority;
	}




	public void setPriority(int priority) {
		this.priority = priority;
	}




	public User getUser() {
		return user;
	}




	public void setUser(User user) {
		this.user = user;
	}






   public int getNoOfTasks() {
		return noOfTasks;
	}




	public void setNoOfTasks(int noOfTasks) {
		this.noOfTasks = noOfTasks;
	}




	public int getCompletedTask() {
		return completedTask;
	}




	public void setCompletedTask(int completedTask) {
		this.completedTask = completedTask;
	}




public boolean isSuspendProject() {
		return suspendProject;
	}




	public void setSuspendProject(boolean suspendProject) {
		this.suspendProject = suspendProject;
	}





	public Project(long projectId, String projectName, Date startDate, Date endDate, int priority, User user,
			int noOfTasks, int completedTask, boolean suspendProject) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.priority = priority;
		this.user = user;
		this.noOfTasks = noOfTasks;
		this.completedTask = completedTask;
		this.suspendProject = suspendProject;
	}




	public Project() {
		 
	 }

	
}
