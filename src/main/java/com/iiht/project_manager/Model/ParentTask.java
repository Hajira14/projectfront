package com.iiht.project_manager.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "ParentTask")
public class ParentTask {
	@Id
	@Column(name= "parentTaskId")
	private long parentTaskId;
	
	@Column(name = "parenttaskname")	
	private String parenttaskname;

	public long getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(long parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public String getParenttaskname() {
		return parenttaskname;
	}

	public void setParenttaskname(String parenttaskname) {
		this.parenttaskname = parenttaskname;
	}

	public ParentTask(long parentTaskId, String parenttaskname) {
		super();
		this.parentTaskId = parentTaskId;
		this.parenttaskname = parenttaskname;
	} 
	public ParentTask() {
		
	}
}


